let numerInput =
  process.argv[2] > 0
    ? process.argv[2]
    : "The number entered should not be negative";

(Factorial = (number) => {
  let reslt = 1;
  for (var index = 2; index <= number; index++) {
    reslt *= index;
  }
  if (reslt === 1) return console.log(numerInput);
  return console.log(reslt);
})(numerInput);
